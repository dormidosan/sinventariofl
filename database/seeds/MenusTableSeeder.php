<?php

use Illuminate\Database\Seeder;
use App\Menu;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $m1 = DB::table('menus')->insertGetId([
            'nombre' => 'Taller',
            'slug' => 'menu',
            'parent' => 0,
            'order' => 1,
            'icon' => 'fa fa-cogs'
        ]);

        $m2 = DB::table('menus')->insertGetId([
            'nombre' => 'Gerencia',
            'slug' => 'menu',
            'parent' => 0,
            'order' => 2,
            'icon' => 'fa fa-file-text-o'
        ]);

        $m3 = DB::table('menus')->insertGetId([
            'nombre' => 'Recepcion',
            'slug' => 'menu',
            'parent' => 0,
            'order' => 3,
            'icon' => 'fa fa-sort-alpha-asc'
        ]);

        $m4 = DB::table('menus')->insertGetId([
            'nombre' => 'Bodega',
            'slug' => 'menu',
            'parent' => 0,
            'order' => 4,
            'icon' => 'fa fa-list-ol'
        ]);

        DB::table('menus')->insertGetId([
            'nombre' => 'Menus',
            'slug' => 'menu',
            'parent' => 0,
            'order' => 0,
            'icon' => ''
        ]);

        //CHILDS

        DB::table('menus')->insertGetId([
            'nombre' => 'Producto y equipo',
            'slug' => 'menu',
            'parent' => $m1,
            'order' => 0,
            'icon' => ''
        ]);

        DB::table('menus')->insertGetId([
            'nombre' => 'Movimiento de equipo',
            'slug' => 'menu',
            'parent' => $m1,
            'order' => 0,
            'icon' => ''
        ]);
        DB::table('menus')->insertGetId([
            'nombre' => 'Fallas equipo',
            'slug' => 'menu',
            'parent' => $m1,
            'order' => 0,
            'icon' => ''
        ]);
        DB::table('menus')->insertGetId([
            'nombre' => 'Contabilidad',
            'slug' => 'menu',
            'parent' => $m2,
            'order' => 0,
            'icon' => ''
        ]);
        DB::table('menus')->insertGetId([
            'nombre' => 'Gerencia general',
            'slug' => 'menu',
            'parent' => $m2,
            'order' => 0,
            'icon' => ''
        ]);
        DB::table('menus')->insertGetId([
            'nombre' => 'Productos',
            'slug' => 'menu',
            'parent' => $m4,
            'order' => 0,
            'icon' => ''
        ]);
        DB::table('menus')->insertGetId([
            'nombre' => 'S y E (inv)',
            'slug' => 'menu',
            'parent' => $m4,
            'order' => 0,
            'icon' => ''
        ]);
        
        DB::table('menus')->insertGetId([
            'nombre' => 'Venta y pedidos',
            'slug' => 'menu',
            'parent' => $m4,
            'order' => 0,
            'icon' => ''
        ]);
        


        // $m1 = factory(Menu::class)->create([
        //     'nombre' => 'Opción 1',
        //     'slug' => 'opcion1',
        //     'parent' => 0,
        //     'order' => 0,
        // ]);
        // factory(Menu::class)->create([
        //     'nombre' => 'Opción 2',
        //     'slug' => 'opcion2',
        //     'parent' => 0,
        //     'order' => 1,
        // ]);
        // $m3 = factory(Menu::class)->create([
        //     'nombre' => 'Opción 3',
        //     'slug' => 'opcion3',
        //     'parent' => 0,
        //     'order' => 2,
        // ]);
        // $m4 = factory(Menu::class)->create([
        //     'nombre' => 'Opción 4',
        //     'slug' => 'opcion4',
        //     'parent' => 0,
        //     'order' => 3,
        // ]);
        // factory(Menu::class)->create([
        //     'nombre' => 'Opción 1.1',
        //     'slug' => 'opcion-1.1',
        //     'parent' => $m1->id,
        //     'order' => 0,
        // ]);
        // factory(Menu::class)->create([
        //     'nombre' => 'Opción 1.2',
        //     'slug' => 'opcion-1.2',
        //     'parent' => $m1->id,
        //     'order' => 1,
        // ]);
        // factory(Menu::class)->create([
        //     'nombre' => 'Opción 3.1',
        //     'slug' => 'opcion-3.1',
        //     'parent' => $m3->id,
        //     'order' => 0,
        // ]);
        // $m32 = factory(Menu::class)->create([
        //     'nombre' => 'Opción 3.2',
        //     'slug' => 'opcion-3.2',
        //     'parent' => $m3->id,
        //     'order' => 1,
        // ]);
        // factory(Menu::class)->create([
        //     'nombre' => 'Opción 4.1',
        //     'slug' => 'opcion-4.1',
        //     'parent' => $m4->id,
        //     'order' => 0,
        // ]);
        // factory(Menu::class)->create([
        //     'nombre' => 'Opción 3.2.1',
        //     'slug' => 'opcion-3.2.1',
        //     'parent' => $m32->id,
        //     'order' => 0,
        // ]);
        // factory(Menu::class)->create([
        //     'nombre' => 'Opción 3.2.2',
        //     'slug' => 'opcion-3.2.2',
        //     'parent' => $m32->id,
        //     'order' => 1,
        // ]);
        // factory(Menu::class)->create([
        //     'nombre' => 'Opción 3.2.3',
        //     'slug' => 'opcion-3.2.3',
        //     'parent' => $m32->id,
        //     'order' => 2,
        // ]);


    }
}
