<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        DB::table('users')->insert([
            'name' => 'myadministrador INV',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123'),
        ]);

        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => 'usuario1@gmail.com',
            'password' => bcrypt('123'),
        ]);

        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => 'usuario2@gmail.com',
            'password' => bcrypt('123'),
        ]);
    }
}
