<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->string('nombre', 100)->nullable();
            $table->string('slug', 150)->nullable();
            $table->unsignedInteger('parent')->nullable()->default('0');
            $table->unsignedTinyInteger('order')->nullable()->default('0');            
            $table->string('icon', 100)->nullable();
            $table->unsignedTinyInteger('main')->nullable()->default('0');
            $table->tinyInteger('enabled')->nullable()->default('1');

            //$table->unique(["slug"], 'slug_UNIQUE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
