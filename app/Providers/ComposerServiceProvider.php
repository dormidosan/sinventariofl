<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
//use Illuminate\View\View;
use App\Menu;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //

        // You can use a class for composer we created earlier
        // you will need NavigationViewComposer@compose method
        // which will be called everythime partials.nav is requested
        // View::composer(
        //     'partials.navigation', 'App\Http\ViewComposers\NavigationViewComposer'
        // );

        //view()->composer('layouts.partials.leftsidecolumn',  'App\Http\ViewComposers\MenuComposer');
        // Or you can use below callback function
        //View::composer('layouts.partials.leftsidecolumn', function ($view) {
        // view()->composer('layouts.partials.leftsidecolumn', function ($view) {
        //     $view->with('menuItems', Menu::where('enabled', '1')
        //         ->orderby('parent')
        //         ->orderby('order')
        //         ->orderby('name')
        //         ->get()
        //     );
        // });

        view()->composer('layouts.partials.leftsidecolumn', function ($view) {
            $view->with('menuItems', Menu::getMenus() );
        });

        
    }
}
