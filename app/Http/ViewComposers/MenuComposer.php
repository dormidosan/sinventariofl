<?php

namespace App\Http\ViewComposers;

//use Illuminate\Support\Facades\Auth;

use Illuminate\View\View;
use App\Menu;

class MenuComposer
{
    public $data = [];
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->data['menu_items'] = Controlador::join('jarvis.funciones as b', 'b.controlador_id', '=', 'jarvis.controladores.id')
        //     ->join('jarvis.permisos as c', 'c.funcion_id', '=', 'b.id')
        //     ->select('jarvis.controladores.*','b.*')
        //     ->where('jarvis.controladores.aplicacion_id',1)
        //     ->where('c.usuario_id','=',Auth::user()->id)
        //     ->whereNull('jarvis.controladores.controlador_id')
        //     ->orderby('controladores.orden')
        //     ->get();
            $this->data['menuItems'] = Menu::where('enabled', '1')->get();
           // dd($this->data);
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('menuItems', end($this->data));
    }
}