<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Factory as Faker;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function inicio()
    {
        return view('inicio.inicio');
    }

    public function productos()
    {
        $faker = Faker::create();
        $datos = collect([]);
        for ($i=0; $i < 100 ; $i++) {             
            $datos->add((object) [
            'id' => $i+1,
            'nombre' => $faker->name,
            'url' => $faker->url
        ]);
        }
        return view('inicio.productos')->with('datos',$datos);
    }

    public function productos_nuevos()
    {
        $faker = Faker::create();
        $productos = collect([]);
        for ($i=0; $i < 10 ; $i++) {             
            $productos->add((object) [
            'id' => $i+1,
            'nombre' => $faker->name,
            'url' => $faker->url
        ]);
        }
        return view('inicio.partials.nuevos')->with('productos',$productos);
    }
}
