@if($item['submenu'] == [])
    @if ($item['parent'] == 0)
        <li class="{{ $item['main']==1 ? 'active' : '' }}" >
            <a href="{{ url($item['slug']) }}">
                <i class="{{ empty($item['icon']) ? 'fa fa-circle-o' : $item['icon'] }}"></i>  
                {{ $item['nombre'] }}
            </a>
        </li>
    @else
        <li class="{{ $item['main']==1 ? 'active' : '' }}" >
            <a href="{{ url($item['slug']) }}">
                <i class="fa fa-circle-o"></i> {{ $item['nombre'] }}
            </a>
        </li>
    @endif
@else
<li class="treeview {{ $item['main']==1 ? 'active' : '' }}">
    @if ($item['parent'] == 0)
        <a href="#">
            <i class="{{ empty($item['icon']) ? 'fa fa-share' : $item['icon'] }}"></i> 
                {{ $item['nombre'] }}
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
    @else
        <a href="#">
            <i class="fa fa-circle-o"></i> {{ $item['nombre'] }}
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
    @endif
    
    <ul class="treeview-menu">
        @forelse($item['submenu'] as $submenu)
            @include('layouts.partials.menuitem', [ 'item' => $submenu ])
        @empty            
        @endforelse
    </ul>
</li>
@endif

