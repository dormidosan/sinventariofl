<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
  <!-- Scripts -->

  <!-- Fonts -->

  <!-- Styles -->

  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('libs')}}/bootstrap/dist/css/bootstrap.min.css">
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('libs')}}/font-awesome/css/font-awesome.min.css">
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" /> -->
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('libs')}}/Ionicons/css/ionicons.min.css">
  <<!-- link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.5.6/css/ionicons.min.css" integrity="sha256-kx16nUTlNRyEpdk7cvpnqblbDbTe/nADul9o2uYeGSw=" crossorigin="anonymous" /> -->
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('libs')}}/datatables.net-bs/css/dataTables.bootstrap.min.css"> 
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap.min.css"/>
  
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap.min.css" integrity="sha256-PbaYLBab86/uCEz3diunGMEYvjah3uDFIiID+jAtIfw=" crossorigin="anonymous" /> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('libs/adminlte')}}/css/AdminLTE.min.css">
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.18/css/AdminLTE.min.css" integrity="sha256-cDsPBSf1bkuw6Jd0bWBmN5HUWNhPJp6lLWNyCSvcKGE=" crossorigin="anonymous" /> -->

  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="{{asset('libs/adminlte')}}/css/skins/_all-skins.min.css">
   <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.18/css/skins/_all-skins.min.css" integrity="sha256-ZlEo/0WbhG/pXIL3zcbJoTW9lFxlmSu8a7syXHfTURo=" crossorigin="anonymous" /> -->
   @yield('styles')
 </head>
 <body class="hold-transition fixed skin-purple sidebar-mini">
  <div class="wrapper">

    @include('layouts.partials.header')  

    @include('layouts.partials.leftsidecolumn')

    @yield('content')

    @include('layouts.partials.footer')  
    

    @include('layouts.partials.controlsidebar')
  <!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
   <!-- <div class="control-sidebar-bg"></div> -->

 </div>
 
 



 <!-- jQuery 3 -->
 <script src="{{asset('libs')}}/jquery/dist/jquery.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js" 
integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" 
crossorigin="anonymous"></script> -->
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('libs')}}/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
<!-- DataTables -->
<script src="{{asset('libs')}}/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{asset('libs')}}/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js" integrity="sha256-t5ZQTZsbQi8NxszC10CseKjJ5QeMw5NINtOXQrESGSU=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap.min.js" integrity="sha256-X/58s5WblGMAw9SpDtqnV8dLRNCawsyGwNqnZD0Je/s=" crossorigin="anonymous"></script> -->


  <!-- SlimScroll -->
  <script src="{{asset('libs')}}/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js" integrity="sha256-qE/6vdSYzQu9lgosKxhFplETvWvqAAlmAuR+yPh/0SI=" crossorigin="anonymous"></script> -->
  <!-- FastClick -->
  <script src="{{asset('libs')}}/fastclick/lib/fastclick.js"></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js" integrity="sha256-t6SrqvTQmKoGgi5LOl0AUy+lBRtIvEJ+++pLAsfAjWs=" crossorigin="anonymous"></script> -->
  <!-- AdminLTE App -->
  <script src="{{asset('libs/adminlte')}}/js/adminlte.min.js"></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.18/js/adminlte.min.js" integrity="sha256-tCcp+FCxI8BTDa6VleHlINji0tuf+xrY76gX5Z/eqps=" crossorigin="anonymous"></script> -->
  <!-- AdminLTE for demo purposes -->
  <!-- <script src="../../dist/js/demo.js"></script> -->
  <!-- page script -->
  @yield('js')
  @yield('scripts')

</body>
</html>
