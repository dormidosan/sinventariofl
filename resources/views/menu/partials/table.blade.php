<div class="box">
  <div class="box-header">
    <h3 class="box-title">Modificacion menu del sitio</h3>
  </div>
  <!-- /.box-header -->
  {{-- <div class="box-body "> --}}
  <div class="box-body table-responsive no-padding">
    
    <table id="menusTable" class="table table-bordered table-striped">
      <thead>
        <th>#</th>
        <th>nombre</th>
        <th>url</th>
        <th>parent</th>
        <th>order</th>
        <th>icon</th>
        <th>main</th>
        <th>enabled</th>
        <th>    </th>
      </thead>
      <tbody>
        @forelse($menus as $menu)
        <tr>
          <td>{{ $menu->id }}</td>
          <td>{{ $menu->nombre }}</td>
          <td>{{ $menu->slug }}</td>
          <td>{{ $menu->parent }}</td>
          <td>{{ $menu->order }}</td>
          <td>{{ $menu->icon }}</td>
          <td>{{ $menu->main }}</td>
          <td>{{ $menu->enabled }}</td> 

          <td>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
            Modificar
            </button>
          </td>          
        </tr>
        @empty
        @endforelse
      </tbody>
    </table>
  </div>
</div>