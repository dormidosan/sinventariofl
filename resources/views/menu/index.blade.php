@extends('layouts.app2')

@section('styles')
<style type="text/css">
  .btn {
    /*border: 2px solid black;*/
    background-color: white;
    color: black;
    /*padding: 14px 28px;*/
    /*font-size: 16px;*/
    cursor: pointer;
  }
  /* Blue */
  .btn-primary {
    border-color: #2196F3;
    color: dodgerblue;
  }

  .btn-primary:hover {
    background: #2196F3;
    color: white;
  }
</style>
@endsection

@section('content')	
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      PAGINA
      <small>PAGINA PEQUEÑA</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">REFE</a></li>
      <li class="active">PAGINA</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">


        @include('menu.partials.table')



        <!-- /.box -->
      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
  $('#menusTable').DataTable( {
    // responsive: true,
    // searching: false,
    lengthChange: false
  } );

  
</script>
@endsection
