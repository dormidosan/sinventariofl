<div class="box">
    <div class="box-header">
      <h3 class="box-title">TABLA INSERTADA POR AJAX</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <table id="example3" class="table table-bordered table-striped">
        <thead>
           <th>#</th>
           <th>nombre</th>
           <th>URL</th>
       </thead>
       <tbody>
           @forelse($productos as $producto)
        <tr>
             <td>{{ $producto->id }}</td>
             <td>{{ $producto->nombre }}</td>
             <td>{{ $producto->url }}</td>
        </tr>
         @empty
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
         @endforelse
     </tbody>
 </table>
</div>
</div>